import React from "react";
import "../styling/main.css";
import kwitterLogoKoala from "../styling/Kwitter_Logo_Koala.png";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class Navbar extends React.Component {
  render() {
    console.log(this.props.pathname);
    return (
      <div className="navWrapper">
        <ul className="navBar">
          <li>
            <img
              src={kwitterLogoKoala}
              alt=""
              className="kwitterLogo"
              id="kwitterLogo"
            />
          </li>
        </ul>

        <ul className="navBarLinks">
          <li
            className={
              this.props.pathname === "/edit" ||
              this.props.pathname === "/home" ||
              this.props.pathname === "/register"
                ? "hide"
                : ""
            }
          >
            <Link to="/" id="loginLink">
              LOGIN
            </Link>
          </li>
          <li
            className={
              this.props.pathname === "/register" ||
              this.props.pathname === "/" ||
              this.props.pathname === "/logout"
                ? "hide"
                : ""
            }
          >
            <Link to="/edit" id="editLink">
              EDIT PROFILE
            </Link>
          </li>
          <li
            className={
              this.props.pathname === "/register" ||
              this.props.pathname === "/" ||
              this.props.pathname === "/logout" ||
              this.props.pathname === "/home"
                ? "hide"
                : ""
            }
          >
            <Link to="/home" id="homeLink">
              HOMEPAGE
            </Link>
          </li>
          <li
            className={
              // this.props.pathname === "/" ||
              // this.props.pathname === "/logout" ||
              this.props.pathname === "/register" ? "hide" : ""
            }
          >
            <Link to="/register" id="signupLink">
              SIGN UP
            </Link>
          </li>
          <li
            className={
              this.props.pathname === "/" ||
              this.props.pathname === "/logout" ||
              this.props.pathname === "/register"
                ? "hide"
                : ""
            }
          >
            <Link to="/logout" id="logoutLink">
              LOGOUT
            </Link>
          </li>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { pathname: state.router.location.pathname };
};

export default connect(mapStateToProps)(Navbar);

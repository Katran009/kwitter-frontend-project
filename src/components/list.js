import React from "react";
import { connect } from "react-redux";
import { likeMessage, unlikeMessage } from "../Redux/types";
import { like, deleteTweet } from "../Actions/action";

const PHOTO_URL = "https://picsum.photos/200?photo=";

class List extends React.Component {
  componentDidMount() {
    this.props.GetUserByID();
  }

  state = { users: [] };

  componentDidMount() {
    fetch("https://intense-dusk-76045.herokuapp.com/users", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      },
      mode: "cors"
    })
      .then(response => response.json())
      .then(response => {
        this.setState({
          users: response.users
        });
      });
  }

  formatDate = date => {
    var monthNames = [
      "",
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];

    var day = date.substring(8, 10);
    var monthIndex = parseInt(date.substring(5, 7), 10);
    var year = date.substring(0, 4);
    var time = "";
    if (date.substring(11, 13) > 12) {
      time = date.substring(11, 13) - 12 + date.substring(13, 16) + " PM";
    } else {
      time = date.substring(11, 16) + " AM";
    }

    return day + " " + monthNames[monthIndex] + " " + year + " at " + time;
  };

  handleLike = (userId, messageId) => () => {
    this.props.dispatch(likeMessage(userId, messageId));
  };
  handleUnlike = messageId => () => {
    console.log(messageId);
    this.props.dispatch(unlikeMessage(messageId));
  };

  delete = event => () => {
    console.log("hello");
    deleteTweet(this.props.message);
  };

  render() {
    const likeTweet = like(this.props.messageId);
    // const deleteTweet = deleteTweet(this.props.messageId);

    return (
      <React.Fragment>
        <div className="ui medium feed segment">
          <div className="event">
            <div className="label">
              <img alt="thumbnail" src={PHOTO_URL} />
            </div>
            <div className="content">
              <div className="date">{this.formatDate(this.props.date)}</div>
              <div>
                <p className="userId"> User: {this.props.userId} </p>
              </div>
              <div>
                <p className="messageId"> Message: {this.props.messageId} </p>
              </div>
              <div className="extra text">{this.props.text}</div>
              <div className="meta">
                <button className="like" type="submit" onClick={likeTweet}>
                  <i className="like icon" /> {this.props.likes.length} Likes
                </button>
              </div>
              <div className="meta">
                <button id="delete-button" color="red" onClick={deleteTweet}>
                  Delete {console.log(this.state)}
                </button>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  loggedInUser: state.loggedInUser,
  displayname: state.userData.displayName,
  username: state.userData.username,
  about: state.userData.about,
  numOfPosts: state.userData.messages.length
});

const mapDispatchToProps = dispatch => {
  return {
    deleteTweet: messageId => dispatch(deleteTweet(messageId))
    // GetUserByID: () => dispatch(getUserByID())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(List);

// deleteAMessage
// button only displays if it's our message
// then we send a message to API that has the message id
// how do we find the message id?
// the message also needs to include the user authorization token
// if you find the code for where a post is created you will find the message authorization token
// after the message has been sent to the API and it has come back successful we need to reload the message feed

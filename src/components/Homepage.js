import React, { Component } from "react";
import { Card, Image, Feed, Input, Button } from "semantic-ui-react";
import kwitterLeafImage from "../styling/Kwitter_Leaf.png";
import { connect } from "react-redux";
import Messages from "./messages.js";
import { addTweet } from "../Actions/action.js";
import { getUserByID } from "../Actions/homepageAction";

class Homepage extends Component {
  componentDidMount() {
    this.props.GetUserByID();
  }

  state = {
    text: ""
  };

  handleOnChange = event => {
    this.setState({
      text: event.target.value
    });
  };

  handleSubmit = event => {
    this.props.addTweet({ message: this.state.text });
    this.setState({
      message: ""
    });
  };

  render() {
    return (
      <div className="userHomepage">
        {console.log(this.state)}
        <div className="userCard">
          <Card>
            <Image src={kwitterLeafImage} id="leafImage" />
            <div className="userHomepageInfo">
              <Card.Content>
                <Card.Header>{this.props.displayname}</Card.Header>
                <Card.Meta>
                  <span className="date">{this.props.username}</span>
                </Card.Meta>
                <Card.Description>{this.props.about}</Card.Description>
              </Card.Content>
              <br />
              <Card.Content extra>{this.props.numOfPosts} posts</Card.Content>
            </div>
          </Card>
        </div>
        <div className="userFeed">
          <Input
            className="newUserPost"
            placeholder="What's new with you?"
            autoFocus
            onChange={this.handleOnChange}
            // value={this.state.text}
          />
          <Button
            id="messageSubmitButton"
            onClick={this.handleSubmit}
            type="submit"
          >
            Submit
          </Button>
          <Feed>
            <hr />
            <Messages />
          </Feed>
          <hr />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loggedInUser: state.loggedInUser,
  displayname: state.userData.displayName,
  username: state.userData.username,
  about: state.userData.about,
  numOfPosts: state.userData.messages.length
});

const mapDispatchToProps = dispatch => {
  return {
    addTweet: tweet => dispatch(addTweet(tweet)),
    GetUserByID: () => dispatch(getUserByID())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Homepage);
